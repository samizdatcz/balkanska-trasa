---
title: "Jak Evropa přivírala dveře uprchlíkům"
perex: "Přesně před rokem se zavřela balkánská cesta, kterou do Evropy proudily desítky tisíc migrantů. Reportéři Českého rozhlasu to sledovali z bezprostřední blízkosti."
description: "Přesně před rokem se zavřela balkánská cesta, kterou do Evropy proudily desítky tisíc migrantů. Reportéři Českého rozhlasu to sledovali z bezprostřední blízkosti."
authors: ["Vojtěch Berger"]
published: "24. února 2017"
socialimg: https://cd-i-hosted-pageflow.codevise.de/_a3115/_e8904/main/image_files/processed_attachments/000/112/055/v1/large/Rozske_zari_2015_3.JPG
url: balkanska-trasa
---

<iframe frameborder="0" scrolling="no" src="https://samizdat.pageflow.io/balkanska-trasa/embed#88083"></iframe>
